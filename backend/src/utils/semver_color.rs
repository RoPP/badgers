pub fn color_from(semver: &str) -> &str {
    match &*semver {
        version
            if version.contains("canary")
                || version.contains("alpha")
                || version.contains("beta") =>
        {
            "cyan"
        }
        version if version.starts_with("0.") => "orange",
        _ => "blue",
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_semver_color() {
        assert_eq!(color_from("0.1.2-canary.0"), "cyan");
        assert_eq!(color_from("1.2.3-canary.0"), "cyan");
        assert_eq!(color_from("1.2.3-alpha.0"), "cyan");
        assert_eq!(color_from("1.2.3-beta.0"), "cyan");
        assert_eq!(color_from("1.2.3"), "blue");
        assert_eq!(color_from("0.1.2"), "orange");
    }
}
