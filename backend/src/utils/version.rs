pub fn version(v: &str) -> String {
    if v.is_empty() {
        "unknown".to_string()
    } else if v.chars().next().unwrap().is_digit(10) {
        format!("v{}", v)
    } else {
        v.to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_semver_color() {
        assert_eq!(version("1.2.3"), "v1.2.3");
        assert_eq!(version("v1.2.3"), "v1.2.3");
        assert_eq!(version("dev-master"), "dev-master");
        assert_eq!(version(""), "unknown");
    }
}
