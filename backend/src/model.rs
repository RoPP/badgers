use axum::{
    http::{HeaderValue, header},
    response::{IntoResponse, Response}, body::Full,
};
use lib::Badge;

pub struct BadgeResponse(pub Badge);

impl IntoResponse for BadgeResponse {
    fn into_response(self) -> Response {
        let mut res = Full::from(self.0.to_string()).into_response();
        res.headers_mut().insert(
            header::CONTENT_TYPE,
            HeaderValue::from_static("image/svg+xml;charset=utf-8"),
        );
        res
    }
}

impl BadgeResponse {
    pub fn new(subject: &str, status: &str, color: &str) -> Self {
        Self(Badge::new(subject, status, color, "standard").unwrap_or_default())
    }
}
