use axum::{extract::Path, response::IntoResponse, routing::get, Router};
use regex::Regex;

use crate::model::BadgeResponse;

use lazy_static::lazy_static;

pub fn router() -> Router {
    Router::new().route("/:request", get(badge))
}

async fn badge(Path(subject_status_color): Path<String>) -> impl IntoResponse {
    if let Some((subject, status, color)) = parse_path(subject_status_color) {
        BadgeResponse::new(&subject, &status, &color)
    } else {
        BadgeResponse::new("404", "no badge found", "red")
    }
}

fn replace_special_chars(s: &str) -> String {
    lazy_static! {
        static ref RE: Regex = Regex::new(r#"(?P<before>[^_])_(?P<after>[^_])"#).unwrap();
    }
    RE.replace_all(s, "$before $after")
        .replace("__", "_")
        .replace("--", "-")
}

fn parse_path(path: String) -> Option<(String, String, String)> {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r"^(?P<subject>(?:.+?|--)*?)-(?P<status>(?:.+?|--)*?)-(?P<color>\w+)$")
                .unwrap();
    }
    let caps = RE.captures(&path)?;

    Some((
        replace_special_chars(caps.name("subject")?.as_str()),
        replace_special_chars(caps.name("status")?.as_str()),
        caps.name("color")?.as_str().to_string(),
    ))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn replace_simple_underscore() {
        assert_eq!(replace_special_chars("foo_bar"), "foo bar".to_string())
    }

    #[test]
    fn replace_double_underscore() {
        assert_eq!(
            replace_special_chars("foo__bar__".into()),
            "foo_bar_".to_string()
        )
    }
    #[test]
    fn replace_double_dash() {
        assert_eq!(
            replace_special_chars("foo--bar--".into()),
            "foo-bar-".to_string()
        )
    }

    #[test]
    fn invalid_path() {
        assert_eq!(parse_path("foo-bar-".into()), None)
    }

    #[test]
    fn subject_status_color() {
        assert_eq!(
            parse_path("foo-bar-blue".into()),
            Some(("foo".to_string(), "bar".to_string(), "blue".to_string())),
        )
    }

    #[test]
    fn subject_status_with_space() {
        assert_eq!(
            parse_path("foo_bar-bla bla-red".into()),
            Some((
                "foo bar".to_string(),
                "bla bla".to_string(),
                "red".to_string()
            )),
        )
    }

    #[test]
    fn subject_with_underscore_status_with_dash() {
        assert_eq!(
            parse_path("foo__bar-bla--bla-yellow".into()),
            Some((
                "foo_bar".to_string(),
                "bla-bla".to_string(),
                "yellow".to_string()
            )),
        )
    }
}
