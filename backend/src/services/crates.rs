use axum::extract::{Path, State};
use axum::{response::IntoResponse, routing::get, Router};
use human_format::Formatter;
use reqwest::{self, Client};
use serde::Deserialize;

use crate::model::BadgeResponse;
use crate::utils::{semver_color, version};

#[derive(Deserialize, Debug)]
struct CrateInfo {
    max_version: String,
    downloads: f64,
    recent_downloads: f64,
}

#[derive(Deserialize, Debug)]
struct CratesIoResponse {
    #[serde(rename = "crate")]
    crate_info: CrateInfo,
}

#[derive(Clone)]
struct CratesClient {
    client: Client,
}

impl CratesClient {
    async fn get_crate_info(&self, name: String) -> CrateInfo {
        let res = self
            .client
            .get(format!("https://crates.io/api/v1/crates/{name}"))
            .send()
            .await
            .unwrap()
            .json::<CratesIoResponse>()
            .await
            .unwrap();

        res.crate_info
    }
}

async fn crate_version(
    Path(name): Path<String>,
    State(client): State<CratesClient>,
) -> impl IntoResponse {
    let crate_info = client.get_crate_info(name).await;
    let version = version::version(&crate_info.max_version);

    BadgeResponse::new(
        "crates.io",
        &version,
        semver_color::color_from(&crate_info.max_version),
    )
}

async fn crate_download(
    Path(name): Path<String>,
    State(client): State<CratesClient>,
) -> impl IntoResponse {
    let crate_info = client.get_crate_info(name).await;
    let download = Formatter::new()
        .with_decimals(0)
        .with_separator("")
        .format(crate_info.downloads);

    BadgeResponse::new("downloads", &download, "green")
}

async fn crate_recent_download(
    Path(name): Path<String>,
    State(client): State<CratesClient>,
) -> impl IntoResponse {
    let crate_info = client.get_crate_info(name).await;
    let recent_downloads = format!(
        "{} latest version",
        Formatter::new()
            .with_decimals(1)
            .with_separator("")
            .format(crate_info.recent_downloads)
    );

    BadgeResponse::new("downloads", &recent_downloads, "green")
}

pub fn router() -> Router {
    let client = CratesClient {
        client: reqwest::Client::builder()
            .user_agent("plop")
            .build()
            .unwrap(),
    };

    Router::new()
        .route("/v/:name", get(crate_version))
        .route("/d/:name", get(crate_download))
        .route("/dl/:name", get(crate_recent_download))
        .with_state(client)
}
