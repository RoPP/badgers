use std::io;
use std::net::SocketAddr;

use axum::{http::StatusCode, routing::get_service, Router};
use tower_http::services::ServeFile;

mod model;
mod services;
mod utils;

use crate::services::badge;
use crate::services::crates;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let app = Router::new()
        .route(
            "/",
            get_service(ServeFile::new("static/index.html")).handle_error(
                |error: io::Error| async move {
                    (
                        StatusCode::INTERNAL_SERVER_ERROR,
                        format!("Unhandled internal error: {}", error),
                    )
                },
            ),
        )
        .nest("/badge", badge::router())
        .nest("/crates", crates::router());

    let addr = SocketAddr::from(([127, 0, 0, 1], 8088));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
