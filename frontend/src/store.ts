import { derived, writable } from "svelte/store";


export const badgePath = writable('badge/one-badge-orange')
export const badgeUrl = derived(
  badgePath,
  $badgePath => $badgePath ? `http://localhost:8088/${$badgePath}` : ''
)
