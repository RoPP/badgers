import { presetForms } from "@julr/unocss-preset-forms";
import { sveltekit } from '@sveltejs/kit/vite';
import presetIcons from '@unocss/preset-icons';
import { extractorSvelte, presetAttributify, presetWind } from 'unocss';
import UnoCSS from 'unocss/vite';
import { defineConfig } from 'vite';

export default defineConfig({
  plugins: [
    UnoCSS(
      {
        presets: [
          presetWind(),
          presetAttributify(),
          presetIcons(),
          presetForms()
        ],
        extractors: [extractorSvelte]
      }),
    sveltekit()
  ]
});
