use actix_web::{web, Responder};

use crate::utils::svg_badge::SvgBadge;

async fn badge_without_color(info: web::Path<(String, String)>) -> impl Responder {
    SvgBadge::new(&info.0, &info.1, "grey").unwrap()
}

async fn badge_with_color(info: web::Path<(String, String, String)>) -> impl Responder {
    SvgBadge::new(&info.0, &info.1, &info.2).unwrap()
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/badge")
            .route("/{subject}/{status}", web::get().to(badge_without_color))
            .route(
                "/{subject}/{status}/{color}",
                web::get().to(badge_with_color),
            ),
    );
}
