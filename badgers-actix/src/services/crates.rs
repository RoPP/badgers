use actix_web::{web, Error, Responder};
use awc::Client;
use human_format::Formatter;
use serde_derive::Deserialize;
use serde_json;

use crate::utils::{semver_color, svg_badge::SvgBadge, version};

#[derive(Deserialize)]
struct CrateInfo {
    max_version: String,
    downloads: usize,
    recent_downloads: usize,
}

#[derive(Deserialize)]
struct CratesIoResponse {
    #[serde(rename = "crate")]
    crate_info: CrateInfo,
}

#[derive(Deserialize)]
struct Info {
    crate_name: String,
}

async fn get_crate_info(name: String) -> Result<CrateInfo, Error> {
    let client = Client::default();
    let mut response = client
        .get(format!("https://crates.io/api/v1/crates/{name}"))
        .insert_header(("User-Agent", "Actix-web"))
        .send()
        .await
        .unwrap();

    let body = response.body().await.unwrap();

    let response: CratesIoResponse = serde_json::from_slice(&body).unwrap();

    Ok(response.crate_info)
}

async fn crate_version(info: web::Path<Info>) -> impl Responder {
    get_crate_info(info.crate_name.clone())
        .await
        .and_then(move |crate_info| {
            let version = version::version(&crate_info.max_version);
            Ok(SvgBadge::new(
                "crates.io",
                &version,
                semver_color::color_from(&crate_info.max_version),
            )
            .unwrap())
        })
}

async fn crate_download(info: web::Path<Info>) -> impl Responder {
    get_crate_info(info.crate_name.clone())
        .await
        .and_then(move |crate_info| {
            let download = Formatter::new().format(crate_info.downloads as f64);
            Ok(SvgBadge::new("downloads", &download, "green").unwrap())
        })
}

async fn crate_recent_download(info: web::Path<Info>) -> impl Responder {
    get_crate_info(info.crate_name.clone())
        .await
        .and_then(move |crate_info| {
            let recent_downloads = format!(
                "{} latest version",
                Formatter::new().format(crate_info.recent_downloads as f64)
            );
            Ok(SvgBadge::new("downloads", &recent_downloads, "green").unwrap())
        })
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/crates")
            .route("/v/{crate_name}", web::get().to(crate_version))
            .route("/d/{crate_name}", web::get().to(crate_download))
            .route("/dl/{crate_name}", web::get().to(crate_recent_download)),
    );
}
