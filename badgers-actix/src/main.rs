use actix_files::NamedFile;
use actix_web::middleware::Logger;
use actix_web::{web, App, HttpRequest, HttpServer, Result};
use std::env;

mod services;
mod utils;

use crate::services::badge;
use crate::services::crates;

async fn index(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("static/index.html")?)
}

#[actix_web::main]>
async fn main() -> std::io::Result<()> {
    env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();

    HttpServer::new(|| {
        App::new()
            .wrap(Logger::default())
            .route("/", web::get().to(index))
            .configure(badge::config)
            .configure(crates::config)
    })
    .bind("127.0.0.1:8088")?
    .run()
    .await
}
