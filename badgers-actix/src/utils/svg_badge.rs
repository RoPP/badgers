use actix_web::{HttpRequest, HttpResponse, Responder, body::BoxBody};

use badgers_lib::Badge;

pub struct SvgBadge {
    pub data: Badge,
}

impl SvgBadge {
    pub fn new(subject: &str, status: &str, color: &str) -> Result<Self, String> {
        match Badge::new(subject, status, color, "standard") {
            Ok(data) => Ok(Self { data }),
            Err(_) => Err("Oops".to_string()),
        }
    }
}

impl Responder for SvgBadge {
    type Body = BoxBody;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        let body = format!("{}", &self.data);
        HttpResponse::Ok()
            .content_type("image/svg+xml;charset=utf-8")
            .body(body)
    }
}
