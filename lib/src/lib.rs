mod color;
mod text_width;

use crate::color::{Color, ParseError};
use crate::text_width::get_text_width;

use std::fmt;
use std::str;

pub enum Style {
    Standard,
    Flat,
}

impl str::FromStr for Style {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "flat" => Ok(Style::Flat),
            "standard" => Ok(Style::Standard),
            _ => Err(ParseError),
        }
    }
}

pub struct Badge {
    subject: String,
    status: String,
    color: Color,
    style: Style,
}

impl Badge {
    pub fn new(subject: &str, status: &str, color: &str, style: &str) -> Result<Badge, ParseError> {
        Ok(Badge {
            subject: subject.to_string(),
            status: status.to_string(),
            color: color.parse()?,
            style: style.parse()?,
        })
    }
}

impl Default for Badge {
    fn default() -> Self {
        Self {
            subject: "404".into(),
            status: "no badge found".into(),
            color: "red".parse().unwrap(),
            style: Style::Flat,
        }
    }
}

impl fmt::Display for Badge {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let subject_width = get_text_width(&self.subject);
        let status_width = get_text_width(&self.status);

        let view_box_width = subject_width + status_width + 200f32;
        let view_box_height = 200;
        let icon_width = view_box_width / 10f32;
        let icon_height = view_box_height / 10;

        let color_hex = &self.color;
        let subject = &self.subject;
        let status = &self.status;

        let status_shadow_x = subject_width + 155f32;
        let status_x = subject_width + 145f32;

        let subject_box_width = subject_width + 100f32;
        let status_box_width = status_width + 100f32;

        let shadow_opacity = match self.style {
            Style::Standard => 0.25f32,
            Style::Flat => 0.1,
        };

        write!(
            f,
            r##"<svg width="{icon_width}" height="{icon_height}" viewBox="0 0 {view_box_width} {view_box_height}" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="{subject}: {status}"><title>{subject}: {status}</title>"##
        )?;

        match self.style {
            Style::Standard => write!(
                f,
                r##"<linearGradient id="a" x2="0" y2="100%"><stop offset="0" stop-opacity=".1" stop-color="#eee"/><stop offset="1" stop-opacity=".1"/></linearGradient><mask id="m"><rect width="{view_box_width}" height="{view_box_height}" rx="30" fill="#fff"/></mask><g mask="url(#m)"><rect width="{subject_box_width}" height="{view_box_height}" fill="#555"/><rect width="{status_box_width}" height="{view_box_height}" fill="{color_hex}" x="{subject_box_width}"/><rect width="{view_box_width}" height="{view_box_height}" fill="url(#a)"/></g>"##
            )?,
            Style::Flat => write!(
                f,
                r##"<g><rect fill="#555" width="{subject_box_width}" height="200"/><rect fill="{color_hex}" x="{subject_box_width}" width="{status_box_width}" height="200"/></g>"##
            )?,
        }

        write!(
            f,
            r##"<g aria-hidden="true" fill="#fff" text-anchor="start" font-family="Verdana,DejaVu Sans,sans-serif" font-size="110"><text x="60" y="148" textLength="{subject_width}" fill="#000" opacity="{shadow_opacity}">{subject}</text><text x="50" y="138" textLength="{subject_width}">{subject}</text><text x="{status_shadow_x}" y="148" textLength="{status_width}" fill="#000" opacity="{shadow_opacity}">{status}</text><text x="{status_x}" y="138" textLength="{status_width}">{status}</text></g></svg>"##
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_flat_badge_rs() {
        let badge = Badge::new("badge", "rs", "blue", "flat").unwrap();
        assert_eq!(
            format!("{badge}"),
            r##"<svg width="64.3" height="20" viewBox="0 0 643 200" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="badge: rs"><title>badge: rs</title><g><rect fill="#555" width="439" height="200"/><rect fill="#007ec6" x="439" width="204" height="200"/></g><g aria-hidden="true" fill="#fff" text-anchor="start" font-family="Verdana,DejaVu Sans,sans-serif" font-size="110"><text x="60" y="148" textLength="339" fill="#000" opacity="0.1">badge</text><text x="50" y="138" textLength="339">badge</text><text x="494" y="148" textLength="104" fill="#000" opacity="0.1">rs</text><text x="484" y="138" textLength="104">rs</text></g></svg>"##
        )
    }

    #[test]
    fn test_standard_badge_rs() {
        let badge = Badge::new("badge", "rs", "blue", "standard").unwrap();
        assert_eq!(
            format!("{badge}"),
            r##"<svg width="64.3" height="20" viewBox="0 0 643 200" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="badge: rs"><title>badge: rs</title><linearGradient id="a" x2="0" y2="100%"><stop offset="0" stop-opacity=".1" stop-color="#eee"/><stop offset="1" stop-opacity=".1"/></linearGradient><mask id="m"><rect width="643" height="200" rx="30" fill="#fff"/></mask><g mask="url(#m)"><rect width="439" height="200" fill="#555"/><rect width="204" height="200" fill="#007ec6" x="439"/><rect width="643" height="200" fill="url(#a)"/></g><g aria-hidden="true" fill="#fff" text-anchor="start" font-family="Verdana,DejaVu Sans,sans-serif" font-size="110"><text x="60" y="148" textLength="339" fill="#000" opacity="0.25">badge</text><text x="50" y="138" textLength="339">badge</text><text x="494" y="148" textLength="104" fill="#000" opacity="0.25">rs</text><text x="484" y="138" textLength="104">rs</text></g></svg>"##
        )
    }
}
