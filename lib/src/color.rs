use lazy_static::lazy_static;
use regex::Regex;

lazy_static! {
    static ref HEX_COLOR_RE: Regex = Regex::new(
        r"^(?P<red>[[:xdigit:]]{1,2})(?P<green>[[:xdigit:]]{1,2})(?P<blue>[[:xdigit:]]{1,2})$"
    )
    .unwrap();
}

#[derive(Debug)]
pub struct ParseError;

pub struct Color(String);

impl std::str::FromStr for Color {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            // FIXME manage css color, hsl, ...
            // https://developer.mozilla.org/en-US/docs/Web/CSS/named-color
            "brightgreen" => Ok(Color("#4c1".into())),
            "green" => Ok(Color("#97ca00".into())),
            "yellowgreen" => Ok(Color("#a4a61d".into())),
            "yellow" => Ok(Color("#dfb317".into())),
            "orange" => Ok(Color("#fe7d37".into())),
            "red" => Ok(Color("#e05d44".into())),
            "blue" => Ok(Color("#007ec6".into())),
            "lightgrey" => Ok(Color("#9f9f9f".into())),
            "grey" => Ok(Color("#999".into())),
            "purple" => Ok(Color("#94e".into())),
            "pink" => Ok(Color("#e5b".into())),
            "cyan" => Ok(Color("#1bc".into())),
            "black" => Ok(Color("#2a2a2a".into())),
            v if HEX_COLOR_RE.is_match(v) => Ok(Color(("#".to_owned() + v).to_lowercase())),
            _ => Err(ParseError),
        }
    }
}

impl std::fmt::Display for Color {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_new_predefined_color() {
        let color = "green".parse();
        assert!(color.is_ok());
        let color: Color = color.unwrap();
        assert_eq!(format!("{color}"), "#97ca00");
    }

    #[test]
    fn test_new_hex_color() {
        let color = "F96854".parse();
        assert!(color.is_ok());
        let color: Color = color.unwrap();
        assert_eq!(format!("{color}"), "#f96854");
    }

    #[test]
    fn test_new_invalid_color() {
        assert!("color".parse::<Color>().is_err());
    }
}
