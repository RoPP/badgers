use lib::Badge;
use clap::{App, Arg};

fn build_cli() -> App<'static, 'static> {
    App::new("macaron")
        // .version(crate_version!())
        .arg(Arg::with_name("SUBJECT").required(true).takes_value(true))
        .arg(Arg::with_name("STATUS").required(true).takes_value(true))
        .arg(
            Arg::with_name("COLOR")
                .long("color")
                .takes_value(true)
                .possible_values(&[
                    "green", "blue", "red", "yellow", "orange", "purple", "pink", "grey", "cyan",
                    "black",
                ])
                .default_value("purple"),
        )
        .arg(
            Arg::with_name("STYLE")
                .long("style")
                .takes_value(true)
                .possible_values(&["standard", "flat"])
                .default_value("standard"),
        )
}

fn main() {
    let matches = build_cli().get_matches();

    let subject = matches.value_of("SUBJECT").unwrap();
    let status = matches.value_of("STATUS").unwrap();
    let color = matches.value_of("COLOR").unwrap();
    let style = matches.value_of("STYLE").unwrap();
    let badge = Badge::new(subject, status, color, style).unwrap();

    print!("{badge}");
}
